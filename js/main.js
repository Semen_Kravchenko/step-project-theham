//TABS
function switchTabs(tabs, tabsCont, active, activeCont) {
    tabs.forEach((el) => {
        el.addEventListener('click', function() {
            tabs.forEach((el) => {
                if(el.classList.contains(active)) {
                    el.classList.remove(active);
                }
            });
            this.classList.add(active);
            const datasetTabs = el.getAttribute('data-tab-name');
            tabsCont.forEach((el) => {
                if(el.classList.contains(datasetTabs)) {
                    el.classList.add(activeCont);
                } else {
                    el.classList.remove(activeCont);
                }
            });
        });
    });
}

const serviceTabs = document.querySelectorAll('.service__tabs-title');
const serviceTabsCont = document.querySelectorAll('.service__tabs-content-item');

switchTabs(serviceTabs, serviceTabsCont, 'service__tabs-active', 'active');

const workTabs = document.querySelectorAll('.work__tabs__list-item');
const workTabsCont = document.querySelectorAll('.grid-mod');

switchTabs(workTabs, workTabsCont, 'work__tabs-active', 'active-grid');

// LOAD MORE
const images = [
    './images/work/web design/web-design6.jpg',
    './images/work/landing page/landing-page2.jpg',
    './images/work/web design/web-design5.jpg',
    './images/work/graphic design/graphic-design12.jpg',
    './images/work/graphic design/graphic-design8.jpg',
    './images/work/graphic design/graphic-design4.jpg',
    './images/work/wordpress/wordpress3.jpg',
    './images/work/wordpress/wordpress2.jpg',
    './images/work/landing page/landing-page7.jpg',
    './images/work/landing page/landing-page6.jpg',
    './images/work/landing page/landing-page3.jpg',
    './images/work/web design/web-design1.jpg',
];

const loadMore = document.querySelector('.load-more');
const allLi = document.querySelectorAll('.hover-mod');

loadMore.addEventListener('click', (evt) => {
    evt.preventDefault();
    const activeElemLi = document.querySelector('.active-grid li');
    const activeElem = document.querySelector('.active-grid');
    const activeElemLiImg = document.querySelector('.active-grid li img');
    const activeElemLiCls = activeElemLi.getAttribute('class');
    const newActiveElemLiCls = activeElemLiCls.split(' ');
    const activeElemLiImgCls = activeElemLiImg.getAttribute('class');
    const margin = document.querySelector('.work');
    let howForNew = [];
    images.forEach((el)=>{
        let li = document.createElement('li');
        let img = document.createElement('img');
        newActiveElemLiCls.forEach((el) => {
            li.classList.add(el, 'hov-for-new');
        });
        img.classList.add(activeElemLiImgCls);
        img.src = `${el}`;
        li.append(img);
        activeElem.append(li);
        howForNew.push(li);
        createHover(howForNew);
    });

    evt.target.remove();
    margin.style.paddingBottom = "100px";
});

//MOUSEOVER / MOUSEOUT


function createHover(elem) {
    elem.forEach((el)=>{
        const div = document.createElement('div');
        const a = document.createElement('a');
        const leftCircle = document.createElement('span');
        const rightCircle = document.createElement('span');
        const title = document.createElement('span');
        const subTitle = document.createElement('span');
        div.classList.add('work__hover');
        a.classList.add('work__hover-link');
        a.href = "#";
        leftCircle.classList.add('left__circle', 'work__circle');
        rightCircle.classList.add('right__circle', 'work__circle');
        title.classList.add('work__hover-title');
        subTitle.classList.add('work__hover-subtitle');
        title.textContent = 'creative design';
        subTitle.textContent = 'Web Design';
        a.append(leftCircle);
        a.append(rightCircle);
        a.append(title);
        a.append(subTitle);
        div.append(a);
        el.append(div);
        el.addEventListener('mouseover', (evt) => {
            if(evt.isTrusted === true) {
                div.style.opacity = "1";
                div.style.transition = '.7s'
            }
        });
        el.addEventListener('mouseout', (evt) => {
            if(evt.isTrusted === true) {
                div.style.opacity = "0";
            }
        });
    });
}

createHover(allLi);

// SLIDER
$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: false,
        centerMode: false,
        focusOnSelect: true
      });

});
